<?php

namespace Utilities\Wallet;
use Illuminate\Support\Facades\Http;

class Wallet
{
    protected $baseUrl, $staticsBaseUrl, $totalDepositUrl, $totalWithdrawUrl;

    public function __construct()
    {
        $this->baseUrl = config('wallet_utility.base_url') . "/wallet/";
        $this->staticsBaseUrl = config('wallet_utility.base_url') . "/statics/";
        $this->totalDepositUrl = $this->staticsBaseUrl . 'total-deposit/';
        $this->totalWithdrawUrl = $this->staticsBaseUrl . 'total-withdraw/';
    }

    public function list(
        int $page = 1, ?int $status_filter = null, ?string $tag_filter = null, ?int $user_id_filter = null,
        ?int $currency_id_filter = null, ?int $created_by_filter = null, ?int $updated_by_filter = null
    )
    {
        return Http::get($this->baseUrl, [
            'page' => $page,
            'status' => $status_filter,
            'tag' => $tag_filter,
            'user_id' => $user_id_filter,
            'currency_id' => $currency_id_filter,
            'created_by' => $created_by_filter,
            'updated_by' => $updated_by_filter
        ])->json();
    }

    public function show(int $wallet_id)
    {
        return Http::get($this->baseUrl . $wallet_id)
            ->json();
    }

    public function create(
        int $status, int $user_id, int $currency_id, ?string $tag = null,
        ?float $balance = null, ?float $min_balance = null, ?float $max_balance = null
    )
    {
        return Http::post($this->baseUrl, [
            'status' => $status,
            'tag' => $tag,
            'user_id' => $user_id,
            'currency_id' => $currency_id,
            'balance' => $balance,
            'min_balance' => $min_balance,
            'max_balance' => $max_balance
        ])->json();
    }

    public function edit(
        ?int $status = null, ?int $user_id = null, ?int $currency_id = null, ?string $tag = null,
        ?float $min_balance = null, ?float $max_balance = null
    )
    {
         return Http::post($this->baseUrl, [
            'status' => $status,
            'tag' => $tag,
            'user_id' => $user_id,
            'currency_id' => $currency_id,
            'min_balance' => $min_balance,
            'max_balance' => $max_balance
        ])->json();
    }

    public function delete(int $wallet_id)
    {
        return Http::delete($this->baseUrl . $wallet_id)
            ->json();
    }

    public function totalDeposit(int $wallet_id)
    {
        return Http::get($this->totalDepositUrl . $wallet_id)
            ->json();
    }

    public function totalWithdraw(int $wallet_id)
    {
        return Http::get($this->totalWithdrawUrl . $wallet_id)
            ->json();
    }
}