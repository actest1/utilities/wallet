<?php

namespace Utilities\Wallet;
use Illuminate\Support\Facades\Http;

class WalletArchive
{
    protected $baseUrl;

    public function __construct()
    {
        $this->baseUrl = config('wallet_utility.base_url') . "/wallet-archive/";
    }

    public function list(
        int $page = 1, ?int $wallet_id = null, ?int $status_filter = null, ?string $tag_filter = null,
        ?int $user_id_filter = null, ?int $currency_id_filter = null, ?int $created_by_filter = null,
    )
    {
        return Http::get($this->baseUrl, [
            'page' => $page,
            'wallet_id' => $wallet_id,
            'status' => $status_filter,
            'tag' => $tag_filter,
            'user_id' => $user_id_filter,
            'currency_id' => $currency_id_filter,
            'created_by' => $created_by_filter,
        ])->json();
    }

    public function show(int $archive_id)
    {
        return Http::get($this->baseUrl . $archive_id)
            ->json();
    }

}