<?php

namespace Utilities\Wallet\Enum;

enum WalletStatus: int
{
    CASE DISABLE = 0;
    CASE ENABLE = 1;

    // Temporary state
    CASE SUSPENDED = 2;
}