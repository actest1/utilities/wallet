<?php

namespace Utilities\Wallet\Enum;

enum WalletMessage: int
{
    case FAIL = 0;
    case SUCCESS = 1;
    case INTERNAL_ERROR = 10;
    case DATA_NOT_FOUND = 11;
    case BAD_DATA = 12;

    // Domain exception codes. starting from 100
    case RECURSIVE_TRANSACTION = 100;
    case CURRENCY_MISMATCH = 101;
    case SOURCE_UNAVAILABLE = 102;
    case DESTINATION_UNAVAILABLE = 103;
    case INSUFFICIENT_BALANCE = 104;
    case MAX_BALANCE = 105;
}