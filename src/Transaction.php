<?php

namespace Utilities\Wallet;
use Illuminate\Support\Facades\Http;

class Transaction
{
    protected $baseUrl;

    public function __construct()
    {
        $this->baseUrl = config('wallet_utility.base_url') . "/transaction/";
    }

    public function list(
        int $page = 1, ?int $status_filter = null, ?int $user_id_filter = null, ?int $wallet_id_filter = null,
        ?int $source_id_filter = null, ?int $destination_id_filter = null,  ?int $created_by_filter = null
    )
    {
        return Http::get($this->baseUrl, [
            'page' => $page,
            'status' => $status_filter,
            'user_id' => $user_id_filter,
            'wallet_id' => $wallet_id_filter,
            'source_id' => $source_id_filter,
            'destination_id' => $destination_id_filter,
            'created_by' => $created_by_filter,
        ])->json();
    }

    public function show(int $transaction_id)
    {
        return Http::get($this->baseUrl . $transaction_id)
            ->json();
    }

    public function make(int $source_id, int $destination_id, float $amount)
    {
        return Http::post($this->baseUrl, [
            'source_id' => $source_id,
            'destination_id' => $destination_id,
            'amount' => $amount
        ])
            ->json();
    }

}