<?php

namespace Utilities\Wallet;
use Illuminate\Support\ServiceProvider;

class WalletServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/wallet_utility.php' => config_path('wallet_utility.php')
        ]);
    }

}